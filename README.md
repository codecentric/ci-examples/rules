# rules

In diesem Beispiel werden einige simple Beispiele für `rules` und die ältere weniger mächtigere `only/except` Syntax beschrieben.

*ACHTUNG*: `rules` kann nicht mit `only` oder `except` im gleichen Job kombiniert werden.

Weitere Informationen:
- [`only/except`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic)
- [`rules`](https://docs.gitlab.com/ee/ci/yaml/#rules-clauses)
- [`rules` Templates](https://docs.gitlab.com/ee/ci/yaml/#workflowrules-templates) zum einbinden in die eigene Pipeline

